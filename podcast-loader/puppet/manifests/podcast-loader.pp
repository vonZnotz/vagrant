# create a new run stage to ensure certain modules are included first
stage { 'pre':
  before => Stage['main']
}

# add the baseconfig module to the new 'pre' run stage
class { 'baseconfig':
  stage => 'pre'
}

file {
  '/var/www/podcast-loader':
    ensure => directory,
    owner => 'vagrant',
    group => 'vagrant',
    mode  => '0644';
}

class { 'apache_vhosts':
  vhostfile => 'podcast-loader.conf'
}

class { 'php':
  packages => [
    'php5',
    'php5-cli',
    'libapache2-mod-php5',
    'php5-common',
    'php5-curl',
    'php5-json',
    'php5-mongo',
    'php5-readline',
    'libssh2-php',
    'php5-xdebug'
  ]
}

include baseconfig, apache, apache_vhosts, php, mysql
