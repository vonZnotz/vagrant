class mongodb {
  package { ['mongodb']:
    ensure => present;
  }

  service { 'mongodb':
    ensure  => running,
    require => Package['mongodb'];
  }

  file {
    '/etc/mongodb.conf':
    source  => 'puppet:///modules/mongodb/mongodb.conf',
    before => Service['mongodb'];
  }

}